
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        path: path.join(__dirname + 'dist'),
        filename: 'bundle.js',
        publicPath: '/'
    },

    devServer: {
        port: 4000,
        historyApiFallback: true
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(eot|otf|ttf|woff|woff2)$/,
                use: 'file-loader',
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test:/\.(png|jpg|gif)$/,
                use: [
                    { loader: 'url-loader'}
                ]
            }
        
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './app/index.html'
        })
    ]

}