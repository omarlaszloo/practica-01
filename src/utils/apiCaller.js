import fetch from 'isomorphic-fetch'

export const API_URL = process.env.SBS_ENV === 'production'
  ? 'http://0.0.0.0:8080/api/v1': 'http://0.0.0.0:8080/api/v1'

const callApi = (endpoint, method = 'get', body, formdata = false) => {
	return fetch(`${API_URL}/${endpoint}`, { method,
		headers: { 'content-type': 'application/json' },
		body: formdata == false ? JSON.stringify(body) : body
	})
	.then(response => response.json()
	.then(json => ({ json, response })))
	.then(({ json, response }) => {
	if (!response.ok) {
		return Promise.reject(json)
	}
	return json
	})
	.then(
	response => response,
	error => error
	)
}



export default callApi
