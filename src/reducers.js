/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import Pcs from './containers/Pcs/reducers'

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
const createRootReducer = history => combineReducers({
  Pcs,
  router: connectRouter(history)
})

export default createRootReducer
