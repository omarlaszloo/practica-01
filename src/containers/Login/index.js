import React from 'react'

import { Formik, Form, Field } from 'formik' 

import InputText from '../../components/InputText'
import Buttons from '../../components/Button'
import './login.scss'
import { login } from './actions'

import { useDispatch, useSelector } from 'react-redux'

const Login = () => {

    const dispatch = useDispatch()
    const actionLogin = async data => await dispatch(login(data))

    const submitLogin = (values) => {
        console.log('login', values)
        actionLogin(values)
    }

    const validations = {
		email: '',
		password: ''
	}

    return (
        <div className="container">
            <div className="container-login">
                <Formik
                    initialValues={validations}
                    validate={values => {
                        const errors = {}
                        if (!values.email) {
                            errors.email = '*Campo requerido'
                        } else if (
                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
                        ) {
                            errors.email = 'email incorrecto'
                        }

                        if (!values.password) {
                            errors.password = '*Campo requerido'
                        }

                        return errors
                    }}
                    onSubmit = {values => submitLogin(values)}
                    render = {({ submitForm }) => (
                        <Form className="main-login">
                            <div className="title">
                                <h2>MAESTROS</h2>
                            </div>
                            <div className="field">    
                                <Field
                                    disabled={false}
                                    type="text"
                                    name="email"
                                    component={InputText}
                                    label="email"
                                />
                            </div>
                            <div className="field">    
                                <Field
                                    disabled={false}
                                    type="password"
                                    name="password"
                                    component={InputText}
                                    label="contraseña"
                                />
                            </div>
                            <div className="btn">
                                <Buttons type="submit" name="iniciar sesión" submitForm={submitForm}/>
                            </div>
                        </Form>
                    )}
                />
            </div>
        </div>
    )
}

export default Login