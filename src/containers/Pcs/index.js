import React, { useState } from 'react'
import TableComponent from '../../components/TableComponent'
import SwipeableTextMobileStepper from '../../components/Carrusel'

import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'

import { actionsAddPcs, actionShowModal, actionsHideModal } from './actions'

import ModalComponent from '../../components/Modal'

import { useDispatch, useSelector } from 'react-redux'

import InputText from '../../components/InputText'

import { Formik, Form, Field } from 'formik' 

import './teachers.scss'

const Teachers = () => {

    const dispatch = useDispatch()
    const add = async data => await dispatch(actionsAddPcs(data))
    const show = () => dispatch(actionShowModal())
    const hide = () => dispatch(actionsHideModal())

    const [formValues, setFormValues] = useState('')
    
    const submit = values => {
        console.log(formValues)
        add(formValues)
    }
    
    const state = useSelector(state => state)

    const handlerShowModal = () => {
        show()
    }

    const validations = {
        branch: '',
        model: '',
        price: '',
        cpu: '',
        ram: '',
        type: '',
        shape: ''
    }

    const { teachers } = state.Pcs


    return (
        <div className="container-lists">

            <ModalComponent
                sizeModal = "sm"
                title = "Agregar Pc"
                btnTitle1 = "Guardar"
                next = {() => submit()}
                showModal = {state.Pcs.showModal}
                hideModal={hide}>
                
                <Formik
					initialValues={validations}
					validate={values => {
						const errors = {}
						if (!values.identification) {
							errors.identification = "*Campo requerido"
						}

                        setFormValues(values)
						return errors
					}}
					onSubmit= {values => {
						console.log('values>>>>>', values)
						
					}}
					render = {( { submitMain }) => (
					
					<Form className="add-seller">
						<div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="branch"
                                component={InputText}
                                label="Brand"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="model"
                                component={InputText}
                                label="Model"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="price"
                                component={InputText}
                                label="prce"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="cpu"
                                component={InputText}
                                label="Cpu"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="ram"
                                component={InputText}
                                label="Ram"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="type"
                                component={InputText}
                                label="Type"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="shape"
                                component={InputText}
                                label="Shape"
                            />
                        </div>
					</Form>
					)}
				/>
            </ModalComponent>
            <div className="container-table">
                <TableComponent data={teachers} />
            
                <div className="container-add">
                    <div className="row">
                        <Fab color="primary" aria-label="add" onClick={() => handlerShowModal()}>
                            <AddIcon />
                        </Fab>
                    </div>
                </div>

                <div className="contianer-carrusel">
                    <stron>Laptops computers carousel</stron>
                    <SwipeableTextMobileStepper />
                </div>
            </div>
            
        </div>
    )
}

export default Teachers