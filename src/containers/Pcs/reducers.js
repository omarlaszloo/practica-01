
import {

    ADD_PCS,
    DELETE_TEACHER,
    SHOW_MODAL,
    HIDE_MODAL
    
} from './constansts'

const initialState = {
    teachers: [],
    showModal: false,
    hideModal: false
}

export default function (state = initialState, action) {
    switch(action.type) {


        case ADD_PCS:
            return {
                ...state,
                teachers: [...state.teachers, action.payload]
            }

        case SHOW_MODAL:
            return {
                ...state,
                showModal: true,
                hideModal: false
            }

        case HIDE_MODAL:
            return {
                ...state,
                showModal: false,
                hideModal: true
            }
        
        default:
            return state
    }
}
