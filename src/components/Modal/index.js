
import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import ClearIcon from '@material-ui/icons/Clear'
import Button from '@material-ui/core/Button'

// component
import Typography from '@material-ui/core/Typography';

import './modals.scss'

const ModalComponent = props => {

    const {
        showModal,
        hideModal,
        children,
        sizeModal,
        title,
        actionBack,
        btnTitle1,
        btnTitle2,
        back,
        next,
        disabled
        
    } = props

    return (
        <Dialog
            open={showModal}
            aria-labelledby="alert-dialog-title"
            fullWidth={sizeModal}
            maxWidth={sizeModal}
            aria-describedby="alert-dialog-description">
            
                <div className="container-m">
                    {/*
                    *
                    * header
                    *
                    */}
                    <div className="header">
                        <div className="title">
                            <Typography variant="h3" component="h3">{title}</Typography>
                        </div>
                        <div className="clear">
                            <ClearIcon
                                color="secondary"
                                fontSize="large"
                                className="hide-modal"
                                onClick={() => hideModal()}
                            />
                        </div>
                    </div>

                    <div className="body">
                        {children}
                    </div>

                <div className="footer">
                    <div className="content-f">
                        {actionBack && <div className="btn-left">
                            <Button
                                variant="outlined"
                                color="secondary"
                                className="btn-f"
                                onClick={() => back()}>
                                {btnTitle2}
                            </Button>
                        </div>}
                        <div className="btn-righ">
                            <Button
                                variant="outlined"
                                color="secondary"
                                className="btn-f"
                                type="submit"
                                disabled={disabled}
                                onClick={() => next()}>
                                {btnTitle1}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </Dialog>
    )
}

export default ModalComponent