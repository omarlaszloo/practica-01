import React from 'react'
import Button from '@material-ui/core/Button'


const Buttons = props => {

    const { name, submitForm } = props

  return (
    <Button variant="outlined" color="primary" onClick={submitForm}>
    { name }
    </Button>
  )
}

export default Buttons 