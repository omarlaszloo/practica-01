import React from 'react'

import { Switch, Route } from 'react-router-dom'

import Login from './containers/Login'
import Pcs from './containers/Pcs'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />
      <Route exact path="/lists" component={Pcs} />

    </Switch>
  )
}

export default App

